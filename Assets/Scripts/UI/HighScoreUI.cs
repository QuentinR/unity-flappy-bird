﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreUI : MonoBehaviour {

    Text HighScore;


	// Use this for initialization
	void Start () {

        HighScore = GetComponent<Text>();

	}
	
	// Update is called once per frame
	void Update () {

        HighScore.text = "HighScore: " + GameControl.instance.HighScore;
	}
}
